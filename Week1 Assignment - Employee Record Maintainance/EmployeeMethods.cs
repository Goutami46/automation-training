using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeData
{
    class Employee
    {
        public int empID;
        public string empName;
        public string empTechnology;

        public void readEmp()
        {

            Console.Write("\n\tEnter Employee Id : ");
            var idAsString = Console.ReadLine();
            bool parseSuccess = int.TryParse(idAsString, out empID);
            if (parseSuccess)
            Console.WriteLine($"ID is: {empID}");
            else
            Console.WriteLine("This is not a number!");
            
             
            Console.Write("\n\tEnter Employee name: ");
            this.empName = Console.ReadLine();
            Console.Write("\n\tEnter Technology: ");
            this.empTechnology = Console.ReadLine();
            
        }
        public void editEmp()
        {
            String editName, editTech;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Your choices are:\n ");
            
                Console.WriteLine("1.Edit Employee Name");
                Console.WriteLine("2.Edit Employee Technology");
                Console.WriteLine("3.Exit");

                Console.WriteLine("Enter your choice");
                int ch = Convert.ToInt32(Console.ReadLine());

                Console.Write("Editing the record:\n");
                switch (ch)
                {
                    case 1:
                            
                            Console.Write("Enter the New name:\n");
                            editName = Console.ReadLine();
                            empName = editName;
                            Console.Write("The new name is " + empName);
                            continue;
                    case 2:
                            Console.Write("Enter the new technology:\n");
                            editTech = Console.ReadLine();
                            empTechnology = editTech;
                            Console.Write("The new Technology is " + empTechnology);
                            continue;

                    case 3:
                            return;

                    default:
                            Console.WriteLine("\n Enter a valid choice\n");
                    break;
                }
            }     
        }
        public void display()
        {
            Console.WriteLine("\n Employee details:\n ");
            Console.Write("\n\t Employee Id is: " + this.empID + "\t Employee Name is: " + this.empName + "\t Technology is: " + this.empTechnology+ "\n");
        }        
    }
}
