﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeData
{
    
    class EmployeeRecordOperation 
    {
        public static void Main(String[] args)
        {
            Console.WriteLine("Enter the number of employees :");
            int num = Convert.ToInt32(Console.ReadLine());
            
            IList<Employee> Emp = new List<Employee>();
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Your choices are:\n ");
            
                Console.WriteLine("1.Add an Employee Record");
                Console.WriteLine("2.Edit an Employee Record");
                Console.WriteLine("3.Delete an Employee Record");
                Console.WriteLine("4.Display an Employee Record");
                

                Console.WriteLine("Enter your choice");
                int choice;
                var choiceAsString = Console.ReadLine();
                if (int.TryParse(choiceAsString, out choice))
                {
                    Console.WriteLine($"Your choice is: {choice}");
                
                    switch (choice)
                    {
                        case 1:
                                for(int i = 0; i < num; i++)
                                {
                                    Console.Write("\nEnter details of Employee" + (i + 1) +"\n");
                                    Employee e = new Employee(); 
                                    e.readEmp();
                                    Emp.Add(e);
                                }
                                break;
                        case 2:
                                
                                for(int j = 0; j < num; j++)
                                {
                                    Console.Write("\nEnter employee ID whose record has to be edited\n");
                                    int num2 = Convert.ToInt32(Console.ReadLine());
                                    int flag = 0;
                                    for(int i = 0; i < num; i++)
                                    {
                                        if(num2 == Emp[i].empID)
                                        {
                                            Emp[i].editEmp();
                                            flag = 1;
                                            break;
                                        }
                                    }
                                    if (flag == 0)                           
                                    {
                                        Console.Write("\nInvalid ID ");
                                    }
                                    break;
                                }
                                break;
                        
                        case 3:
                                for(int j = 0; j < num; j++)
                                {
                                    Console.Write("\nEnter employee ID whose record has to be deleted\n");
                                    int del = Convert.ToInt32(Console.ReadLine());
                                    int flag = 0;
                                    for(int i = 0; i < num; i++)
                                    {
                                        if(del == Emp[i].empID)
                                        {
                                            Console.Write("Deleting");
                                            Emp.Remove(Emp[i]);
                                            flag = 1;
                                            break;
                                        }   
                                    }
                                    if (flag == 0)
                                    {
                                        Console.Write("\n Invalid ID");
                                    }
                                    break;
                                }
                                break;
                        case 4:
                            
                                for(int i = 0; i < num; i++)
                                {
                                    Emp[i].display();
                                    Console.WriteLine("press a key :");
                                    Console.ReadKey();
                                }
                                break; 

                        default:
                                Console.WriteLine("\n Enter a valid choice\n");
                        break;
                    }
                    
                }
                else
                {
                    Console.WriteLine("Invalid choice!");
                }                     
            }       
        } 
    }   
}

   
