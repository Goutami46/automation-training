﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileOperations
{
    public class FileOperation
    {
        public static void Main(string[] args)
        {
            string fileName = @"D:\\testfile"; 
            int count;
    
            while (true)
            {
                Console.Clear();
                Console.WriteLine("\n Choices are:");
                Console.WriteLine("1. Create a file");
                Console.WriteLine("2. Write to the file");
                Console.WriteLine("3. append a line ");
                Console.WriteLine("4. count the number of lines in the file");
                Console.WriteLine("5. print the last line in the file");
                Console.WriteLine("6. delete the file");
                Console.WriteLine("7. display");
                Console.WriteLine("8. Exit");

                Console.WriteLine("Select your choice: ");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1: Console.WriteLine("Creating the file\n");
                            create();
                            break;
 
                    case 2: Console.WriteLine("Writing lines in the file \n");
                            write();
                            break;
 
                    case 3:  Console.WriteLine("Appending a line to the file\n");
                            append();
                            break;
 
                    case 4: Console.WriteLine("Counting number of lines \n");
                            linesCount();
                            break;

                    case 5: Console.WriteLine("Printing the last line\n");
                            lastLine();
                            break;

                    case 6: Console.WriteLine("File deleted \n");
                            delete();
                            break;
                    case 7: Console.WriteLine( "Displaying content:");
                            display();
                            break;

                    case 8: System.Environment.Exit(1);
                            break;

                    default: Console.WriteLine("Enter a valid choice");
                             break;
                }
                Console.ReadKey();
            }



            void create()
            {
                if (!File.Exists(fileName)) 
                {
                    FileStream Filestr = new FileStream(fileName, FileMode.Create);                
                    Filestr.Close();                 
                }
                Console.Write("File has been created and the Path is D:\\testfile");
                Console.ReadKey();           
            }



            void write()
            {
                if (File.Exists(fileName))
                {
                    using (StreamWriter writer = File.CreateText(fileName))  
                    {  
                            writer.WriteLine("Line 1");  
                            writer.WriteLine("Line 2");  
                            writer.WriteLine("Line 3");  
                            writer.WriteLine("Line 4");  
                            writer.WriteLine("Line 5");  
                    }       
                    string readText = File.ReadAllText(fileName);  
                    Console.WriteLine(readText);
                }
                else
                {
                    Console.WriteLine("File not found!");
                }
            }        
        


           void append()
           {
                if (File.Exists(fileName))
                {
                    Console.Write("\n\n Append some text to an existing file  :\n");
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, true))
                    {
                        file.WriteLine(" This is the line appended at last line.");
                    }
                    using (StreamReader reader = File.OpenText(fileName))
                    {
                        string string1 = "";
                        Console.WriteLine(" Here is the content of the file after appending the text : ");
                        while ((string1 = reader.ReadLine()) != null)
                        {
                            Console.WriteLine(string1);
                        }
                        Console.WriteLine("");
                    }  
                }
                else
                {
                    Console.WriteLine("File not found!");
                }
           }



            void linesCount()
           {
                if (File.Exists(fileName))
                {
                    using (StreamReader reader = File.OpenText(fileName))
                    {
                        string string2 = "";
                        count = 0;
                        Console.WriteLine(" Here is the content of the file testfile : ");
                        while ((string2 = reader.ReadLine()) != null)
                        {
                            Console.WriteLine(string2);
                            count++;   
                        }
                        Console.WriteLine("");
                    }
                    Console.Write(" The number of lines in  the file {0} is : {1} \n\n", fileName, count);
                }
                else
                {
                    Console.WriteLine("File not found!");
                }
           }
            
  

            void lastLine()
            {
                if (File.Exists(fileName))
                { 
                    using (StreamReader reader = File.OpenText(fileName))
                    {
                        string string3 = "";
                        String last = "";
                        Console.WriteLine(" Here is the last line of the file: ");
                        Console.WriteLine("--------------------------------------------");
                        while ((string3 = reader.ReadLine()) != null)
                        {
                             last = File.ReadLines(@"D:\\testfile").Last();   
                        }
                        Console.WriteLine(last);   
                        Console.WriteLine("");
                    }
                }
                else
                {
                    Console.WriteLine("File not found!");
                }
            }



            void display()
            {
                if (File.Exists(fileName))
                {
                    using (StreamReader reader = File.OpenText(fileName))
                    {
                        string string4 = "";
                        Console.WriteLine(" Here is the content of the file testfile : ");
                        while ((string4 = reader.ReadLine()) != null)
                        {
                            Console.WriteLine(string4);
                        }
                        Console.WriteLine("");
                    }
                }
                else
                {
                    Console.WriteLine("File not found!");
                }
            }




            void delete()
            { 
                try
                {
                    File.Delete(fileName);
                    using (StreamReader reader = File.OpenText(fileName))
                    {
                        string string5 = "";
                        Console.WriteLine(" Here is the content of the file testfile : ");
                        while ((string5 = reader.ReadLine()) != null)
                        {
                            Console.WriteLine(string5);
                        }
                        Console.WriteLine("");
                    }

                }        
                catch(FileNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
                   
            }  
        } 
    }
}